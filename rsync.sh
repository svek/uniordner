#/bin/sh
# Rsync-Script um mirror auf sven.koeppel.org vom  Uni-Ordner vom FIAS umzusetzen.
# $Id$

# This is not supposed for CGI usage
[ $REQUEST_URI ] && echo -e "Content-Type: text/plain\n\nNo CGI script" && exit

# change to directory of script (nice for cron usage, etc.)
cd `dirname $0`

# Slashes: in order to copy directory contents, make sure the trailing
# slash is at the SOURCE only.

# really make sure the target folder != .
# otherwise --delete will wipe out this script and anything else
TARGET='sven@technikum29.de:/home/sven/sko/sven.koeppel.org/uni/mirror'

# source at ITP
SOURCE='/home/koeppel/UniOrdner/'


# The target charset on this machine (source is utf-8, omit if we
# are using UTF-8)
#CHARSET='--iconv="ISO_8859-15"'
CHARSET=''

# This is over ssh.
# The password is just to keep stupid people outside
#export RSYNC_PASSWORD="geheimespasswort"

rsync -az --delete --stats --log-file='rsync.log' ${CHARSET} ${SOURCE} ${TARGET}

# this is so stupid:

# afterwards make some log output
##echo "This is the syncer running on $(hostname) at $(date)" > mirror.txt
##echo -n "Last sync: `date -R`" >> mirror.txt

# and another output
##date -R | cat footer.html.template - > footer.html

# this was it.
