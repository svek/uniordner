# The UniOrdner infrastructure and website #

This git repository was started in Sept 29, 2015 and replaces the older SVN repository at https://svn.technikum29.de/private-websites/uni-ordner/

It contains 

* Most important, the *per-directory permission system for Apache*, written with mod_perl, capable of skinned index pages, etc. (Features should be collected here, including installation instructions) - This system is useful in general and may be moved to a dedicated repository, thought.
* The UniOrdner starting website (plain HTML + design files)
* Scripts to organize the UniOrdner structure (to come)

What is not contained in this repository:

* All the actual content files of the UniOrdner (roughtly 10GB) are not contained in this repository (or any other).

## Before releasing this code public ##

* All real-name connections (*Sven Köppel*) should be removed
* Texts about Philipsen, Rischke should be changed in some way or are *not* supposed to be put on http://github.com/svenk