#!/usr/bin/python
#
# This is a proof of concept (May 4, 2015)
#
# This script uses bibtexparser (https://bibtexparser.readthedocs.org/),
# installed as elearning@itp: easy_install --user bibtexparser
# Usage: cat something.bib | ./bib2json.py
# Output on stdout.

# could also use `import fileinput` but this is linewise

import sys
fhandle = sys.stdin

import bibtexparser
db = bibtexparser.load(fhandle)
entries_list = db.entries


import json

print json.dumps(entries_list)
